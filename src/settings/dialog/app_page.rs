use crate::settings::{Settings, UserDataSize};
use crate::util::{BuilderHelper, GtkUtil, Util};
use crate::{
    app::Action,
    settings::general::{KeepArticlesDuration, SyncInterval},
};
use futures::{channel::oneshot, FutureExt};
use gdk::{EventMask, EventType};
use glib::{clone, object::Cast, translate::ToGlib, Sender};
use gtk::{
    prelude::WidgetExtManual, Button, ButtonExt, EventBox, Inhibit, Label, LabelExt, ListBox, ListBoxExt,
    ListBoxRowExt, Popover, PopoverExt, Settings as GtkSettings, SettingsExt as GtkSettingsExt, Switch, SwitchExt,
    WidgetExt,
};
use libhandy::{ActionRow, PreferencesPage, PreferencesWindow};
use news_flash::NewsFlash;
use parking_lot::RwLock;
use std::sync::Arc;

pub struct AppPage {
    pub widget: PreferencesPage,
    delete_signal: Arc<RwLock<Option<usize>>>,
    sender: Sender<Action>,
    settings: Arc<RwLock<Settings>>,
    news_flash: Arc<RwLock<Option<NewsFlash>>>,

    keep_running_switch: Switch,
    keep_running_signal: Arc<RwLock<Option<usize>>>,

    dark_theme_switch: Switch,
    dark_theme_signal: Arc<RwLock<Option<usize>>>,
    gtk_dark_theme_signal: Arc<RwLock<Option<usize>>>,

    sync_label: Label,
    sync_pop: Popover,
    sync_list: ListBox,
    sync_list_signal: Arc<RwLock<Option<usize>>>,
    sync_listbox_signal: Arc<RwLock<Option<usize>>>,
    sync_event: EventBox,
    sync_event_signal: Arc<RwLock<Option<usize>>>,
    sync_row: ActionRow,

    limit_articles_label: Label,
    limit_articles_pop: Popover,
    limit_articles_list: ListBox,
    limit_articles_list_signal: Arc<RwLock<Option<usize>>>,
    limit_articles_listbox_signal: Arc<RwLock<Option<usize>>>,
    limit_articles_event: EventBox,
    limit_articles_event_signal: Arc<RwLock<Option<usize>>>,
    limit_articles_row: ActionRow,

    clear_cache_button: Button,
    clear_cache_button_signal: Arc<RwLock<Option<usize>>>,

    user_data_label: Label,
    cache_label: Label,
}

impl AppPage {
    pub fn new(
        parent: &PreferencesWindow,
        sender: &Sender<Action>,
        settings: &Arc<RwLock<Settings>>,
        news_flash: &Arc<RwLock<Option<NewsFlash>>>,
    ) -> Self {
        let builder = BuilderHelper::new("settings_app");
        let app_page = builder.get::<PreferencesPage>("app_page");

        // -----------------------------------------------------------------------
        // keep running
        // -----------------------------------------------------------------------
        let keep_running_switch = builder.get::<Switch>("keep_running_switch");
        keep_running_switch.set_state(settings.read().get_keep_running_in_background());

        // -----------------------------------------------------------------------
        // dark theme
        // -----------------------------------------------------------------------
        let dark_theme_switch = builder.get::<Switch>("dark_theme_switch");
        dark_theme_switch.set_state(settings.read().get_prefer_dark_theme());

        // -----------------------------------------------------------------------
        // sync interval
        // -----------------------------------------------------------------------
        let sync_label = builder.get::<Label>("sync_label");
        sync_label.set_label(&settings.read().get_sync_interval().to_string());

        let sync_pop = builder.get::<Popover>("sync_pop");
        let sync_list = builder.get::<ListBox>("sync_list");

        let sync_event = builder.get::<EventBox>("sync_event");
        sync_event.set_events(EventMask::BUTTON_PRESS_MASK);

        let sync_row = builder.get::<ActionRow>("sync_row");

        // -----------------------------------------------------------------------
        // keep articles duration
        // -----------------------------------------------------------------------
        let limit_articles_label = builder.get::<Label>("limit_articles_label");
        if let Some(news_flash) = news_flash.read().as_ref() {
            limit_articles_label
                .set_label(&KeepArticlesDuration::from_duration(news_flash.get_keep_articles_duration()).to_string());
        }

        let limit_articles_pop = builder.get::<Popover>("limit_articles_pop");
        let limit_articles_list = builder.get::<ListBox>("limit_articles_list");

        let limit_articles_event = builder.get::<EventBox>("limit_articles_event");
        limit_articles_event.set_events(EventMask::BUTTON_PRESS_MASK);

        let limit_articles_row = builder.get::<ActionRow>("sync_row");

        // -----------------------------------------------------------------------
        // clear cache
        // -----------------------------------------------------------------------
        let clear_cache_button = builder.get::<Button>("clear_cache_button");

        // -----------------------------------------------------------------------
        // user data display
        // -----------------------------------------------------------------------
        let user_data_label = builder.get::<Label>("user_data_label");
        let cache_label = builder.get::<Label>("cache_label");

        let app_page = AppPage {
            widget: app_page,
            delete_signal: Arc::new(RwLock::new(None)),
            sender: sender.clone(),
            settings: settings.clone(),
            news_flash: news_flash.clone(),

            // -----------------------------------------------------------------------
            // keep running
            // -----------------------------------------------------------------------
            keep_running_switch,
            keep_running_signal: Arc::new(RwLock::new(None)),

            // -----------------------------------------------------------------------
            // dark theme
            // -----------------------------------------------------------------------
            dark_theme_switch,
            dark_theme_signal: Arc::new(RwLock::new(None)),
            gtk_dark_theme_signal: Arc::new(RwLock::new(None)),

            // -----------------------------------------------------------------------
            // sync interval
            // -----------------------------------------------------------------------
            sync_label,
            sync_pop,
            sync_list,
            sync_list_signal: Arc::new(RwLock::new(None)),
            sync_listbox_signal: Arc::new(RwLock::new(None)),
            sync_event,
            sync_event_signal: Arc::new(RwLock::new(None)),
            sync_row,

            // -----------------------------------------------------------------------
            // keep articles duration
            // -----------------------------------------------------------------------
            limit_articles_label,
            limit_articles_pop,
            limit_articles_list,
            limit_articles_list_signal: Arc::new(RwLock::new(None)),
            limit_articles_listbox_signal: Arc::new(RwLock::new(None)),
            limit_articles_event,
            limit_articles_event_signal: Arc::new(RwLock::new(None)),
            limit_articles_row,

            // -----------------------------------------------------------------------
            // clear cache
            // -----------------------------------------------------------------------
            clear_cache_button,
            clear_cache_button_signal: Arc::new(RwLock::new(None)),

            user_data_label,
            cache_label,
        };
        app_page.setup_close(parent);
        app_page.setup_data_section();
        app_page.setup_interaction();
        app_page
    }

    fn setup_close(&self, parent: &PreferencesWindow) {
        self.delete_signal.write().replace(
            parent.connect_delete_event(clone!(
                    @strong self.delete_signal as delete_signal,

                    @weak self.keep_running_switch as keep_running_switch,
                    @strong self.keep_running_signal as keep_running_signal,
                    @weak self.dark_theme_switch as dark_theme_switch,
                    @strong self.dark_theme_signal as dark_theme_signal,
                    @strong self.gtk_dark_theme_signal as gtk_dark_theme_signal,

                    @weak self.sync_list as sync_list,
                    @strong self.sync_list_signal as sync_list_signal,
                    @weak self.sync_row as sync_row,
                    @strong self.sync_listbox_signal as sync_listbox_signal,
                    @weak self.sync_event as sync_event,
                    @strong self.sync_event_signal as sync_event_signal,

                    @weak self.limit_articles_list as limit_articles_list,
                    @strong self.limit_articles_list_signal as limit_articles_list_signal,
                    @weak self.limit_articles_row as limit_articles_row,
                    @strong self.limit_articles_listbox_signal as limit_articles_listbox_signal,
                    @weak self.limit_articles_event as limit_articles_event,
                    @strong self.limit_articles_event_signal as limit_articles_event_signal,

                    @weak self.clear_cache_button as clear_cache_button,
                    @strong self.clear_cache_button_signal as clear_cache_button_signal => @default-panic, move |dialog, _event| {
                        GtkUtil::disconnect_signal(*delete_signal.read(), dialog);

                        GtkUtil::disconnect_signal(*keep_running_signal.read(), &keep_running_switch);
                        GtkUtil::disconnect_signal(*dark_theme_signal.read(), &dark_theme_switch);
                        GtkUtil::disconnect_signal(*sync_list_signal.read(), &sync_list);
                        GtkUtil::disconnect_signal(*sync_event_signal.read(), &sync_event);
                        GtkUtil::disconnect_signal(*limit_articles_list_signal.read(), &limit_articles_list);
                        GtkUtil::disconnect_signal(*limit_articles_event_signal.read(), &limit_articles_event);
                        GtkUtil::disconnect_signal(*clear_cache_button_signal.read(), &clear_cache_button);
                        if let Some(gtk_settings) = GtkSettings::get_default() {
                            GtkUtil::disconnect_signal(*gtk_dark_theme_signal.read(), &gtk_settings);
                            gtk_dark_theme_signal.write().take();
                        }
                        if let Some(listbox) = sync_row.get_parent() {
                            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                                GtkUtil::disconnect_signal(*sync_listbox_signal.read(), &listbox);
                                sync_listbox_signal.write().take();
                            }
                        }
                        if let Some(listbox) = limit_articles_row.get_parent() {
                            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                                GtkUtil::disconnect_signal(*limit_articles_listbox_signal.read(), &listbox);
                                limit_articles_listbox_signal.write().take();
                            }
                        }

                        delete_signal.write().take();

                        keep_running_signal.write().take();
                        dark_theme_signal.write().take();
                        sync_list_signal.write().take();
                        sync_event_signal.write().take();
                        limit_articles_list_signal.write().take();
                        limit_articles_event_signal.write().take();
                        clear_cache_button_signal.write().take();

                        Inhibit(false)
                    }
                )
            ).to_glib() as usize
        );
    }

    fn setup_interaction(&self) {
        self.keep_running_signal.write().replace(
            self.keep_running_switch
                .connect_state_set(clone!(
                    @weak self.settings as settings,
                    @strong self.sender as sender => @default-panic, move |_switch, is_set|
                {
                    if settings.write().set_keep_running_in_background(is_set).is_err() {
                        Util::send(
                            &sender,
                            Action::ErrorSimpleMessage("Failed to set setting 'keep running'.".to_owned()),
                        );
                    }
                    Inhibit(false)
                }))
                .to_glib() as usize,
        );

        self.dark_theme_signal.write().replace(
            self.dark_theme_switch
                .connect_state_set(clone!(
                    @weak self.settings as settings,
                    @strong self.sender as sender => @default-panic, move |_switch, is_set| {
                    if settings.write().set_prefer_dark_theme(is_set).is_ok() {
                        if let Some(settings) = GtkSettings::get_default() {
                            if settings.get_property_gtk_application_prefer_dark_theme() != is_set {
                                settings.set_property_gtk_application_prefer_dark_theme(is_set);
                            }
                        }
                    } else {
                        Util::send(
                            &sender,
                            Action::ErrorSimpleMessage("Failed to set setting 'dark theme'.".to_owned()),
                        );
                    }
                    Inhibit(false)
                }))
                .to_glib() as usize,
        );

        if let Some(gtk_settings) = GtkSettings::get_default() {
            self.gtk_dark_theme_signal.write().replace(gtk_settings.connect_property_gtk_application_prefer_dark_theme_notify(clone!(
                @weak self.dark_theme_switch as dark_theme_switch  => @default-panic, move |gtk_settings|
            {
                if gtk_settings.get_property_gtk_application_prefer_dark_theme() != dark_theme_switch.get_state() {
                    dark_theme_switch.set_state(gtk_settings.get_property_gtk_application_prefer_dark_theme());
                }
            })).to_glib() as usize);
        }

        self.sync_list_signal.write().replace(
            self.sync_list
                .connect_row_activated(clone!(
                    @weak self.settings as settings,
                    @weak self.sync_pop as sync_pop,
                    @weak self.sync_label as sync_label,
                    @strong self.sender as sender => @default-panic, move |_list, row| {
                    sync_pop.popdown();
                    let sync_interval = match row.get_index() {
                        0 => SyncInterval::Never,
                        2 => SyncInterval::QuaterHour,
                        4 => SyncInterval::HalfHour,
                        6 => SyncInterval::Hour,
                        8 => SyncInterval::TwoHour,

                        _ => SyncInterval::Never,
                    };
                    sync_label.set_label(&sync_interval.to_string());
                    if settings.write().set_sync_interval(sync_interval).is_ok() {
                        Util::send(&sender, Action::ScheduleSync);
                    } else {
                        Util::send(
                            &sender,
                            Action::ErrorSimpleMessage("Failed to set setting 'sync interval'.".to_owned()),
                        );
                    }
                }))
                .to_glib() as usize,
        );

        self.sync_event_signal.write().replace(
            self.sync_event
                .connect_button_press_event(clone!(
                    @weak self.sync_pop as sync_pop => @default-panic, move |_eventbox, event|
                {
                    if event.get_button() != 1 {
                        return Inhibit(false);
                    }
                    match event.get_event_type() {
                        EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                            return Inhibit(false);
                        }
                        _ => {}
                    }

                    sync_pop.popup();
                    Inhibit(false)
                }))
                .to_glib() as usize,
        );

        if let Some(listbox) = self.sync_row.get_parent() {
            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                self.sync_listbox_signal.write().replace(
                    listbox
                        .connect_row_activated(
                            clone!(@weak self.sync_pop as sync_pop => @default-panic, move |_list, row| {
                                if row.get_widget_name() == "sync_row" {
                                    sync_pop.popup();
                                }
                            }),
                        )
                        .to_glib() as usize,
                );
            }
        }

        self.limit_articles_list_signal.write().replace(
            self.limit_articles_list
                .connect_row_activated(clone!(
                    @strong self.news_flash as news_flash,
                    @weak self.limit_articles_pop as limit_articles_pop,
                    @weak self.limit_articles_label as limit_articles_label,
                    @strong self.sender as sender => @default-panic, move |_list, row| {
                    limit_articles_pop.popdown();
                    let limit_articles_duration = match row.get_index() {
                        0 => KeepArticlesDuration::Forever,
                        2 => KeepArticlesDuration::OneYear,
                        4 => KeepArticlesDuration::SixMonths,
                        6 => KeepArticlesDuration::OneMonth,
                        8 => KeepArticlesDuration::OneWeek,

                        _ => KeepArticlesDuration::Forever,
                    };
                    limit_articles_label.set_label(&limit_articles_duration.to_string());
                    if let Some(news_flash) = news_flash.read().as_ref() {
                        if news_flash.set_keep_articles_duration(limit_articles_duration.to_duration()).is_err() {
                            Util::send(
                                &sender,
                                Action::ErrorSimpleMessage("Failed to set setting 'limit articles duration'.".to_owned()),
                            );
                        }
                    }
                }))
                .to_glib() as usize,
        );

        self.limit_articles_event_signal.write().replace(
            self.limit_articles_event
                .connect_button_press_event(clone!(
                    @weak self.limit_articles_pop as limit_articles_pop => @default-panic, move |_eventbox, event|
                {
                    if event.get_button() != 1 {
                        return Inhibit(false);
                    }
                    match event.get_event_type() {
                        EventType::ButtonRelease | EventType::DoubleButtonPress | EventType::TripleButtonPress => {
                            return Inhibit(false);
                        }
                        _ => {}
                    }

                    limit_articles_pop.popup();
                    Inhibit(false)
                }))
                .to_glib() as usize,
        );

        if let Some(listbox) = self.limit_articles_row.get_parent() {
            if let Ok(listbox) = listbox.downcast::<ListBox>() {
                self.limit_articles_listbox_signal.write().replace(
                    listbox
                        .connect_row_activated(
                            clone!(@weak self.limit_articles_pop as limit_articles_pop => @default-panic, move |_list, row| {
                                if row.get_widget_name() == "limit_articles_row" {
                                    limit_articles_pop.popup();
                                }
                            }),
                        )
                        .to_glib() as usize,
                );
            }
        }

        self.clear_cache_button_signal.write().replace(
            self.clear_cache_button
                .connect_clicked(clone!(@weak self.user_data_label as user_data_label,
                    @weak self.cache_label as cache_label,
                    @strong self.sender as sender => @default-panic, move |button| {

                    button.set_sensitive(false);
                    let (oneshot_sender, receiver) = oneshot::channel::<()>();
                    Util::send(&sender, Action::ClearCache(oneshot_sender));

                    let glib_future = receiver.map(clone!(
                        @weak user_data_label,
                        @weak cache_label,
                        @weak button,
                        @strong sender => @default-panic, move |res| match res {
                        Ok(()) => {
                            Self::query_data_sizes(&sender, &user_data_label, &cache_label);
                            button.set_sensitive(true);
                        },
                        Err(_) => {},
                    }));
                    Util::glib_spawn_future(glib_future);
                }))
                .to_glib() as usize,
        );
    }

    fn setup_data_section(&self) {
        Self::query_data_sizes(&self.sender, &self.user_data_label, &self.cache_label);
    }

    fn query_data_sizes(sender: &Sender<Action>, user_data_label: &Label, cache_label: &Label) {
        let (oneshot_sender, receiver) = oneshot::channel::<UserDataSize>();
        Util::send(sender, Action::QueryDiskSpace(oneshot_sender));
        let glib_future = receiver.map(clone!(
            @weak user_data_label,
            @weak cache_label  => @default-panic, move |res| match res {
            Ok(user_data_size) => {
                user_data_label.set_text(&Util::format_data_size(user_data_size.database.on_disk));
                cache_label.set_text(&Util::format_data_size(user_data_size.webkit));
            }
            Err(_) => {
                let msg = "Failed to query";
                user_data_label.set_text(msg);
                log::warn!("Receiving db size failed.");
            },
        }));
        Util::glib_spawn_future(glib_future);
    }
}
